package com.example.punkapp.helpers

import kotlin.reflect.KProperty1

class ReflectorHelper {
    companion object {
        @Suppress("UNCHECKED_CAST")
        fun <R> readInstanceProperty(instance: Any, propertyName: String): R {
            val property: KProperty1<Any, *> = instance::class.members
                .first { it.name == propertyName } as KProperty1<Any, *>

            // force a invalid cast exception if incorrect type here
            return property.get(instance) as R
        }
    }
}