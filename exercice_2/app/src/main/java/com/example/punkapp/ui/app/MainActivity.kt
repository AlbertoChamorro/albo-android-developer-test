package com.example.punkapp.ui.app

import android.os.Bundle
import android.view.KeyEvent
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import androidx.navigation.ui.navigateUp
import com.example.punkapp.R
import com.example.punkapp.databinding.ActivityMainBinding
import com.example.punkapp.ui.base.BaseActivity

/**
 * class [MainActivity].
 */
class MainActivity : BaseActivity() {

    val mainViewModel: MainViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding: ActivityMainBinding =
            DataBindingUtil.setContentView(
                this,
                R.layout.activity_main
            )
        binding.viewModel = mainViewModel
        binding.lifecycleOwner = this

        mainViewModel.title.observe(this, Observer { title ->
            supportActionBar?.title = title
        })

        mainViewModel.showBackButton.observe(this, Observer { showBackButton ->
            supportActionBar?.setDisplayHomeAsUpEnabled(showBackButton)
        })

        this.rootView = findViewById(R.id.root_view)
    }

    // Have NavigationUI handle up behavior in the ActionBar
    override fun onSupportNavigateUp(): Boolean {

        // Allows NavigationUI to support proper up navigation or the drawer layout, drawer menu, depending on the situation
        super.appBarConfiguration?.let {
            return findNavController(R.id.main_host_fragment).navigateUp(it)
        }

        return super.onSupportNavigateUp()
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        if (onKeyDownFragment != null) {
            return onKeyDownFragment!!(keyCode, event)
        }
        return this.onKeyDownParent(keyCode, event)
    }

    fun onKeyDownParent(keyCode: Int, event: KeyEvent?) = super.onKeyDown(keyCode, event)
}


