package com.example.punkapp.repositories.base

import androidx.lifecycle.LiveData
import com.example.punkapp.PunkApp
import com.example.punkapp.common.components.SingleSourceOfTruthStrategy
import com.example.punkapp.common.services.ResultReactiveData
import com.example.punkapp.helpers.ConnectionHelper

abstract class BaseRepository {
    fun <TSource, TNet> resultLiveData(
        databaseQuery: (suspend () -> LiveData<ResultReactiveData<TSource>>)? = null,
        networkCall: (suspend () -> ResultReactiveData<TNet>)? = null,
        saveCallResult: (suspend (TNet?) -> Unit)? = null,
        onlyOffline: Boolean = false
    ): LiveData<ResultReactiveData<TSource>> {

        if (!onlyOffline) {
            // with network connection
            val context = PunkApp.shared?.baseContext
            if (ConnectionHelper.isInternetAvailable(context)) {
                return SingleSourceOfTruthStrategy.resultLiveData(
                    null,
                    networkCall,
                    saveCallResult
                )
            }
        }

        // without network connection / or features that are requires with offline
        return SingleSourceOfTruthStrategy.resultLiveData<TSource, TNet>(
            databaseQuery,
            null,
            null
        )
    }
}