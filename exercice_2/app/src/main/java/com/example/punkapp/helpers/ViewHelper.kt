package com.example.punkapp.helpers

import android.content.Context
import android.content.res.ColorStateList
import android.graphics.drawable.Drawable
import android.graphics.drawable.GradientDrawable
import android.text.Html
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.text.method.LinkMovementMethod
import android.view.View
import android.view.ViewTreeObserver.OnGlobalLayoutListener
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import androidx.annotation.ColorInt
import androidx.appcompat.app.AppCompatActivity
import com.example.punkapp.R
import com.example.punkapp.common.services.PagedQueryString


class ViewHelper {
    companion object {
        fun dismissKeyboard(activity: AppCompatActivity) {
            val inputMethodManager: InputMethodManager? =
                activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
            inputMethodManager?.hideSoftInputFromWindow(
                activity.currentFocus?.windowToken,
                0
            )
            activity.window.decorView.clearFocus()
        }

        fun dismissKeyboard(activity: AppCompatActivity, view: View) {
            val inputMethodManager: InputMethodManager? =
                activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
            inputMethodManager?.hideSoftInputFromWindow(
                view.windowToken,
                0
            )
            activity.window.decorView.clearFocus()
        }

        fun getItemDecorator(
            context: Context,
            @ColorInt colorRes: Int
        ): Drawable {
            val stateDrawable = GradientDrawable()
            val colorStateList = ColorStateList.valueOf(colorRes)
            val borderSize = context.resources.getDimension(R.dimen.borderless_button).toInt()
            stateDrawable.color = colorStateList.withAlpha((0.65 * 255).toInt())
            stateDrawable.setSize(borderSize, borderSize)
            return stateDrawable
        }

        fun createPagedQueryString(model: PagedQueryString): MutableMap<String, String> {
            val queryMap: MutableMap<String, String> = HashMap()
            queryMap["page"] = model.page.toString()
            queryMap["per_page"] = model.pageSize.toString()
            queryMap["Search"] = model.search
            return queryMap
        }


        fun makeTextViewResizable(
            tv: TextView,
            maxLine: Int,
            expandText: String,
            viewMore: Boolean
        ) {
            if (tv.tag == null) {
                tv.tag = tv.text
            }
            val vto = tv.viewTreeObserver
            vto.addOnGlobalLayoutListener(object : OnGlobalLayoutListener {
                override fun onGlobalLayout() {
                    val obs = tv.viewTreeObserver
                    obs.removeGlobalOnLayoutListener(this)
                    if (maxLine == 0) {
                        val lineEndIndex = tv.layout.getLineEnd(0)
                        val text =
                            tv.text.subSequence(0, lineEndIndex - expandText.length + 1)
                                .toString() + " " + expandText
                        tv.text = text
                        tv.movementMethod = LinkMovementMethod.getInstance()
                        tv.setText(
                            addClickablePartTextViewResizable(
                                Html.fromHtml(tv.text.toString()), tv, maxLine, expandText,
                                viewMore
                            ), TextView.BufferType.SPANNABLE
                        )
                    } else if (maxLine > 0 && tv.lineCount >= maxLine) {
                        val lineEndIndex = tv.layout.getLineEnd(maxLine - 1)
                        val text =
                            tv.text.subSequence(0, lineEndIndex - expandText.length + 1)
                                .toString() + " " + expandText
                        tv.text = text
                        tv.movementMethod = LinkMovementMethod.getInstance()
                        tv.setText(
                            addClickablePartTextViewResizable(
                                Html.fromHtml(tv.text.toString()), tv, maxLine, expandText,
                                viewMore
                            ), TextView.BufferType.SPANNABLE
                        )
                    } else {
                        val lineEndIndex =
                            tv.layout.getLineEnd(tv.layout.lineCount - 1)
                        val text =
                            tv.text.subSequence(0, lineEndIndex).toString() + " " + expandText
                        tv.text = text
                        tv.movementMethod = LinkMovementMethod.getInstance()
                        tv.setText(
                            addClickablePartTextViewResizable(
                                Html.fromHtml(tv.text.toString()),
                                tv,
                                lineEndIndex,
                                expandText,
                                viewMore
                            ), TextView.BufferType.SPANNABLE
                        )
                    }
                }
            })
        }

        private fun addClickablePartTextViewResizable(
            strSpanned: Spanned, tv: TextView,
            maxLine: Int, spanableText: String, viewMore: Boolean
        ): SpannableStringBuilder? {
            val str = strSpanned.toString()
            val ssb = SpannableStringBuilder(strSpanned)
            if (str.contains(spanableText)) {
                ssb.setSpan(object : SpannableView(false) {
                    override fun onClick(widget: View) {
                        if (viewMore) {
                            tv.layoutParams = tv.layoutParams
                            tv.setText(tv.tag.toString(), TextView.BufferType.SPANNABLE)
                            tv.invalidate()
                            makeTextViewResizable(tv, -1, "Leer Menos", false)
                        } else {
                            tv.layoutParams = tv.layoutParams
                            tv.setText(tv.tag.toString(), TextView.BufferType.SPANNABLE)
                            tv.invalidate()
                            makeTextViewResizable(tv, 3, "...Leer Más", true)
                        }
                    }
                }, str.indexOf(spanableText), str.indexOf(spanableText) + spanableText.length, 0)
            }
            return ssb
        }
    }
}