package com.example.punkapp.common.services

import com.google.gson.annotations.SerializedName

data class PagedResponse<T>(

    @SerializedName("records")
    var records: List<T> = listOf(),

    @SerializedName("pageSize")
    val pageSize: Int = 0,

    @SerializedName("currentPage")
    val currentPage: Int = 0,

    @SerializedName("count")
    val count: Int = 0
)

open class PagedQueryString(
    val pageSize: Int = 20,
    val page: Int = 1,
    val search: String = ""
)
