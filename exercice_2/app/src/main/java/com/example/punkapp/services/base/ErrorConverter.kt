package com.example.punkapp.services.base

import android.content.Context
import android.text.TextUtils
import android.util.Log
import com.example.punkapp.R
import com.example.punkapp.common.services.ErrorCode
import com.example.punkapp.common.services.GenericAPIError
import com.example.punkapp.helpers.extensions.fromValue
import com.example.punkapp.helpers.toJson
import retrofit2.HttpException
import retrofit2.Response

class ErrorUtils {

    companion object {

        // https://medium.com/programming-lite/retrofit-2-handling-network-error-defc7d373ad1
        fun onHandleError(
            response: Response<*>? = null,
            exception: Throwable? = null,
            context: Context
        ): GenericAPIError? {
            val error: GenericAPIError?

            when {
                response != null -> {
                    error =
                        deserializeResponseError(
                            response
                        )
                }
                exception is HttpException -> {
                    error =
                        deserializeResponseError(
                            exception.response()
                        )
                }
                else -> {
                    error = if (TextUtils.isEmpty(exception?.message)) {
                        Log.d(ErrorUtils::class.java.simpleName, "Network failure connection")
                        GenericAPIError(
                            ErrorCode.NETWORK_CONNECTION_ERROR,
                            context.resources.getString(R.string.network_error),
                            ErrorCode.GENERIC_ERROR.code
                        )
                    } else {
                        Log.d(ErrorUtils::class.java.simpleName, "Generic error")

                        val isAttemptFailureConnect =
                            exception?.message?.contains("Connect", ignoreCase = true)
                        GenericAPIError(
                            if (isAttemptFailureConnect == true) ErrorCode.NETWORK_CONNECTION_ERROR
                            else ErrorCode.GENERIC_ERROR,
                            if (isAttemptFailureConnect == true) context.resources.getString(R.string.network_error)
                            else context.resources.getString(R.string.generic_error),
                            if (isAttemptFailureConnect == true) ErrorCode.NETWORK_CONNECTION_ERROR.code
                            else ErrorCode.GENERIC_ERROR.code
                        )
                    }
                }
            }

            return error
        }

        private fun deserializeResponseError(response: Response<*>?): GenericAPIError? {
            val responseString = response?.errorBody()?.string() ?: ""
            return buildResponseError(responseString, response?.code())
        }

        private fun buildResponseError(
            responseString: String,
            httpStatusCode: Int?
        ): GenericAPIError? {
            val json = responseString.toJson()

            json?.apply {
                val customBusinessCode = get("code")?.asInt ?: 0
                var message = get("message")?.asString ?: ""
                val errorList = get("errors")?.asJsonArray
                errorList?.let { errors ->
                    if (errors.size() > 0) {
                        message = if (errorList.size() == 1) {
                            errorList.get(0).asString
                        } else {
                            errorList.map { jsonElement -> jsonElement.asString }
                                .reduce { acc, item -> "${acc}\n$item" }
                        }
                    }
                }

                return GenericAPIError(
                    errorCode = ErrorCode::class.fromValue(value = customBusinessCode)
                        ?: ErrorCode.GENERIC_ERROR,
                    message = message,
                    httpStatusCode = httpStatusCode ?: 0
                )
            }

            return null
        }
    }
}
