package com.example.punkapp.helpers

import com.google.gson.Gson
import com.google.gson.JsonObject


// https://www.baeldung.com/gson-string-to-jsonobject
fun String.toJson(): JsonObject? {

    // convertedObject["name"].asString
    val jsonObject = Gson().fromJson(this, JsonObject::class.java)
    return if (jsonObject.isJsonObject) {
        return jsonObject
    } else null
}
