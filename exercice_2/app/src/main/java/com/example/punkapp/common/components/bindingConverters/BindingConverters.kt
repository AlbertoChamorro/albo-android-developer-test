package com.example.punkapp.common.components.bindingConverters

import android.view.View
import androidx.databinding.BindingConversion
import androidx.recyclerview.widget.RecyclerView
import com.example.punkapp.common.services.ResultReactiveData

object BindingConverters {

    @BindingConversion
    @JvmStatic
    fun booleanToVisibility(isVisible: Boolean): Int {
        return if (isVisible) View.VISIBLE else View.GONE
    }

    @BindingConversion
    @JvmStatic
    fun reactiveStatusToVisibility(status: ResultReactiveData.Status?): Int {
        if (status == null) return View.GONE

        return when (status) {
            ResultReactiveData.Status.ERROR, ResultReactiveData.Status.SUCCESS_WITHOUT_DATA -> View.VISIBLE
            else -> View.GONE
        }
    }

    @BindingConversion
    @JvmStatic
    fun adapterToVisibility(adapter: RecyclerView.Adapter<*>?): Int {
        if (adapter == null) return View.GONE

        return if (adapter.itemCount > 0) View.VISIBLE else View.GONE
    }
}
