package com.example.punkapp.ui.customViews

import android.annotation.SuppressLint
import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import com.example.punkapp.R
import com.example.punkapp.databinding.CustomLoadingViewBinding

class LoadingView constructor(
    context: Context,
    attrs: AttributeSet?,
    defStyle: Int
) : ConstraintLayout(context, attrs, defStyle) {

    private lateinit var mBinding: CustomLoadingViewBinding

    constructor(context: Context) : this(context, null, 0)

    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0) {
        initViews(context)
    }

    @SuppressLint("ResourceAsColor")
    private fun initViews(context: Context) {
        val layoutInflater =
            context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        mBinding = CustomLoadingViewBinding.inflate(layoutInflater, this, true)
        setDefaultValues()
    }

    private fun setDefaultValues() {
        messageIn = null
        showMessageIn = null
        progressHeightIn = null
        progressWidthIn = null
        progressTintColorIn = null
    }

    // inputs / outputs
    private val messageInDefault = context.resources.getString(R.string.loading)
    var messageIn: String? = null
        set(value) {
            field = value ?: messageInDefault
            mBinding.message = value ?: messageInDefault
        }

    private val showMessageInDefault = true
    var showMessageIn: Boolean? = null
        set(value) {
            field = value ?: showMessageInDefault
            mBinding.isShowMessage = value ?: showMessageInDefault
        }

    private val progressHeightInDefault =
        context.resources.getDimension(R.dimen.height_progress_bar)
    var progressHeightIn: Float? = null
        set(value) {
            field = value ?: progressHeightInDefault
            mBinding.progressHeightIn = value ?: progressHeightInDefault
        }

    private val progressWidthInDefault = context.resources.getDimension(R.dimen.height_progress_bar)
    var progressWidthIn: Float? = null
        set(value) {
            field = value ?: progressWidthInDefault
            mBinding.progressWidthIn = value ?: progressWidthInDefault
        }

    private val progressTintColorInDefault = ContextCompat.getColor(context, R.color.colorAccent)
    var progressTintColorIn: Int? = null
        set(value) {
            field = value ?: progressTintColorInDefault
            mBinding.progressTintColorIn = value ?: progressTintColorInDefault
        }

    // functions
    private fun setMessage(value: CharSequence?) {
        mBinding.baseLoadingMessageTextView.text = value
    }

    fun hiddenProgressBar(isHidden: Boolean) {
        mBinding.baseLoadingProgressBar.visibility = if (isHidden) View.GONE else View.VISIBLE
    }

    fun isHiddenProgressBar(): Boolean = mBinding.baseLoadingProgressBar.visibility != View.VISIBLE

    fun hiddenView(isHidden: Boolean) {
        this.visibility = if (isHidden) View.GONE else View.VISIBLE
    }

    fun isHiddenView(): Boolean = this.visibility != View.VISIBLE
}