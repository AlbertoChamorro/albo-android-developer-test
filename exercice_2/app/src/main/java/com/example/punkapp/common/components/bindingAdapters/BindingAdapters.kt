package com.example.punkapp.common.components.bindingAdapters

import android.annotation.SuppressLint
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.DrawableRes
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.example.punkapp.R
import com.example.punkapp.common.services.ResultReactiveData
import com.example.punkapp.helpers.ViewHelper
import com.example.punkapp.ui.customViews.LoadingView
import com.squareup.picasso.Picasso

object BindingAdapters {

    // https://medium.com/@msasikanth/recyclerviews-made-easy-1be23275cdca
    @BindingAdapter("customAdapter")
    @JvmStatic
    fun setAdapter(
        recyclerView: RecyclerView,
        adapter: RecyclerView.Adapter<*>?
    ) {
        if (adapter != null) {
            val layoutManager = LinearLayoutManager(recyclerView.context)
            layoutManager.orientation = LinearLayoutManager.VERTICAL
            recyclerView.layoutManager = layoutManager
            recyclerView.adapter = adapter
        }
    }

    @BindingAdapter("list_divider_item_orientation")
    @JvmStatic
    fun setDividerItemDecoration(
        recyclerView: RecyclerView,
        orientation: String?
    ) {
        orientation?.let {
            val decorator = DividerItemDecoration(
                recyclerView.context,
                if (it == "H") DividerItemDecoration.HORIZONTAL else DividerItemDecoration.VERTICAL
            )
            val color = ContextCompat.getColor(recyclerView.context, R.color.colorRipple)
            ViewHelper.getItemDecorator(recyclerView.context, color)
                .let { drawable ->
                    decorator.setDrawable(drawable)
                }
            recyclerView.addItemDecoration(decorator)
        }
    }

    @SuppressLint("DefaultLocale")
    @BindingAdapter("capitalize")
    @JvmStatic
    fun setCapitalize(view: View, text: String?) {
        if (view is EditText) {
            if (view.text.toString() != text) {
                view.setText(text?.capitalize())
            }
        } else if (view is TextView) {
            if (view.text.toString() != text) {
                view.text = text?.capitalize()
            }
        }
    }

    @BindingAdapter("width")
    @JvmStatic
    fun setCustomWidthView(view: View, value: Float?) {
        if (value != null) {
            if (view.layoutParams.width != value.toInt()) {
                view.layoutParams.width = value.toInt()
                view.invalidate()
            }
        }
    }

    @BindingAdapter("height")
    @JvmStatic
    fun setCustomHeightView(view: View, value: Float?) {
        if (value != null) {
            if (view.layoutParams.height != value.toInt()) {
                view.layoutParams.height = value.toInt()
                view.invalidate()
            }
        }
    }

    @BindingAdapter("baseLoading_hiddenProgressBar")
    @JvmStatic
    fun hiddenBaseLoadingProgressBar(customView: LoadingView, isHidden: Boolean) {
        if (isHidden != customView.isHiddenView()) {
            customView.hiddenView(isHidden)
        }
    }

    @BindingAdapter("baseLoading_hiddenProgressBar")
    @JvmStatic
    fun hiddenBaseLoadingProgressBar(
        customView: LoadingView,
        status: ResultReactiveData.Status?
    ) {
        if (status == null) {
            customView.hiddenView(true)
            return
        }

        val criterionHiddenView = status != ResultReactiveData.Status.LOADING
        if (criterionHiddenView != customView.isHiddenView()) {
            customView.hiddenView(criterionHiddenView)
        }
    }

    @BindingAdapter("swipeRefresh_enabled")
    @JvmStatic
    fun enableSwipeRefreshGesture(
        view: SwipeRefreshLayout,
        enabled: Boolean?
    ) {
        val isEnabled = enabled != null && enabled
        if (isEnabled != view.isEnabled) {
            view.isEnabled = isEnabled
            view.isRefreshing = isEnabled
        }
    }

    @BindingAdapter(
        value = ["readMore_enable", "readMore_maxLine", "readMore_TextButton"],
        requireAll = false
    )
    @JvmStatic
    fun setReadMore(
        view: TextView?,
        isEnable: Boolean,
        maxLine: Int?,
        textMore: String?
    ) {
        if (isEnable && view != null) {
            ViewHelper.makeTextViewResizable(view, maxLine ?: 3, textMore ?: "...Leer Más", true)
        }
    }
}

object ImageViewBindingAdapter {

    @BindingAdapter("android:src")
    @JvmStatic
    fun setImageUri(imageView: ImageView, imageUri: String?) {
        Picasso
            .get()
            .load(imageUri)
            .placeholder(R.drawable.placeholder)
            .error(R.drawable.placeholder)
            .into(imageView)
    }

    @BindingAdapter("android:src")
    @JvmStatic
    fun setImageResource(imageView: ImageView, @DrawableRes resource: Int?) {
        if (resource != null)
            imageView.setImageResource(resource)
    }
}
