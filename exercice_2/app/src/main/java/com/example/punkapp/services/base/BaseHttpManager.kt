package com.example.punkapp.services.base

import com.example.punkapp.services.beers.BeersRemoteSource
import com.example.punkapp.services.beers.IBeers

class BaseHttpManager {

    companion object {
        val shared: BaseHttpManager by lazy { BaseHttpManager() }
    }

    val service: ServiceGenerator by lazy { ServiceGenerator() }

    // remotes services
    val beersRemoteSource: BeersRemoteSource by lazy {
        BeersRemoteSource(service.createService(IBeers::class.java))
    }
}