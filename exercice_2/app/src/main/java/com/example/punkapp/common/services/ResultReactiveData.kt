package com.example.punkapp.common.services

data class ResultReactiveData<out T>(
    val status: Status,
    val data: T?,
    val error: GenericAPIError?
) {

    enum class Status {
        SUCCESS,
        ERROR,
        LOADING,

        // if web api return without data | filter with searchView without data
        SUCCESS_WITHOUT_DATA
    }

    companion object {
        fun <T> success(data: T?): ResultReactiveData<T> {
            return ResultReactiveData(Status.SUCCESS, data, null)
        }

        fun <T> successWithoutData(data: T?): ResultReactiveData<T> {
            return ResultReactiveData(Status.SUCCESS_WITHOUT_DATA, data, null)
        }

        fun <T> error(error: GenericAPIError?, data: T? = null): ResultReactiveData<T> {
            return ResultReactiveData(Status.ERROR, data, error)
        }

        fun <T> loading(data: T? = null): ResultReactiveData<T> {
            return ResultReactiveData(Status.LOADING, data, null)
        }
    }
}