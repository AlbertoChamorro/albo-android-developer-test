package com.example.punkapp.common.services

@Suppress("unused")
class GenericAPIError(val errorCode: ErrorCode, val message: String, val httpStatusCode: Int) {

    constructor() : this(ErrorCode.GENERIC_ERROR, "", ErrorCode.GENERIC_ERROR.code)
}

@Suppress("unused")
enum class ErrorCode(val code: Int) {
    NETWORK_CONNECTION_ERROR(-100),
    GENERIC_ERROR(0),
    GENERIC_LIST_ERROR(100),

    BAG_REQUEST(400),
    BUSINESS_BAG_REQUEST(4000),

    NOT_AUTHORIZED(401),
    BUSINESS_NOT_AUTHORIZED(4001),

    NOT_PERMISSION(403),
    BUSINESS_NOT_PERMISSION(4003),

    NOT_FOUND(404),
    BUSINESS_NOT_FOUND(4004),

    INTERNAL_SERVER(500),
    BUSINESS_INTERNAL_SERVER(5000),
}