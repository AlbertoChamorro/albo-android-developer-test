package com.example.punkapp.ui.app

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.paging.ExperimentalPagingApi
import androidx.paging.LoadState
import com.example.punkapp.R
import com.example.punkapp.databinding.FragmentHomeBinding
import com.example.punkapp.ui.app.beers.BeersViewModel
import com.example.punkapp.ui.base.BaseFragment
import com.example.punkapp.ui.lists.BeersAdapter
import com.example.punkapp.ui.lists.ListLoadStateAdapter
import kotlinx.android.synthetic.main.custom_list_view.view.*
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

class HomeFragment : BaseFragment() {

    private lateinit var binding: FragmentHomeBinding
    private val beersViewModel: BeersViewModel by viewModels()
    private val adapter by lazy {
        BeersAdapter(
            this.beersViewModel
        )
    }

    private var searchBeersJob: Job? = null
    private fun getBeers() {
        // Make sure we cancel the previous job before creating a new one
        searchBeersJob?.cancel()
        searchBeersJob = lifecycleScope.launch {
            @OptIn(ExperimentalPagingApi::class)
            beersViewModel.getBeers().collectLatest {
                adapter.submitData(it)
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false)
        binding.lifecycleOwner = this
        binding.viewModel = beersViewModel
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        this.initViews()
    }

    private fun initViews() {
        // set Title app
        baseActivity.mainViewModel.setTitle(resources.getString(R.string.app_name))

        binding.listView.error_view.iconIn = null
        binding.listView.base_refresh_view.setOnRefreshListener {
            beersViewModel.currentSearchResult = null
            adapter.notifyDataSetChanged()
            getBeers()
        }

        beersViewModel.beerIdArgDestination.observe(
            viewLifecycleOwner,
            Observer { id ->
                val bundle =
                    bundleOf("id" to id)
                findNavController().navigate(R.id.beerDetailFragment, bundle)
            })

        initAdapter()
        getBeers()
    }

    private fun initAdapter() {
        binding.adapter = adapter.withLoadStateHeaderAndFooter(
            footer = ListLoadStateAdapter { adapter.retry() },
            header = ListLoadStateAdapter { adapter.retry() },
        )

        adapter.addLoadStateListener { loadState ->
            // Only show the list if refresh succeeds.
            Log.d("[AddLoadStateListener]", "state -> $loadState")
            binding.listView.recyclerView.isVisible =
                loadState.source.refresh is LoadState.NotLoading
            // Show loading spinner during initial load or refresh.
            binding.listView.base_loading_view.isVisible =
                loadState.source.refresh is LoadState.Loading
            // Show the retry state if initial load or refresh fails.
            binding.listView.error_view.isVisible =
                (loadState.source.refresh is LoadState.Error) || adapter.itemCount == 0

            if ((loadState.source.refresh is LoadState.Error ||
                        loadState.source.refresh is LoadState.NotLoading) &&
                binding.listView.base_refresh_view.isRefreshing
            ) {
                binding.listView.base_refresh_view.isRefreshing = false
            }

            // Toast on any error, regardless of whether it came from RemoteMediator or PagingSource
            val errorState = loadState.source.append as? LoadState.Error
                ?: loadState.source.prepend as? LoadState.Error
                ?: loadState.append as? LoadState.Error
                ?: loadState.prepend as? LoadState.Error
            errorState?.let {
                this.notifyError(
                    message = "\uD83D\uDE28 Sorry! :( ${it.error}"
                )
            }
        }
    }
}