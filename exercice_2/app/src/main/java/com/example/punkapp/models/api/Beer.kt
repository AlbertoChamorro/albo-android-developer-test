package com.example.punkapp.models.api

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity(tableName = "beers")
data class Beer(
    @PrimaryKey @field:SerializedName("id") val id: Long,
    @field:SerializedName("name") val name: String,
    @field:SerializedName("description") val description: String?,
    @field:SerializedName("first_brewed") val firstBrewed: String?,
    @field:SerializedName("tagline") val tagLine: String?,
    @field:SerializedName("image_url") val pictureUrl: String?,
    @field:SerializedName("food_pairing") val foodPairing: List<String>?
)