package com.example.punkapp.common.components.observables

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.punkapp.common.components.emitters.Event
import com.example.punkapp.common.components.emitters.SingleLiveEvent
import com.example.punkapp.common.services.ErrorCode
import com.example.punkapp.common.services.GenericAPIError

enum class RenderModeError {
    // show error in container with snackBar
    DEFAULT,

    // Embed error in view inside list
    LIST
}

open class FormViewModel<TRepository>(application: Application) :
    BaseObservableViewModel<TRepository>(application) where TRepository : Any {

    // Defined if form contain errors to validations, if mIsFormValid == true, can enable controls UI for example buttons
    private val _isFormValid = SingleLiveEvent<Boolean>()
    val isFormValid: LiveData<Boolean>
        get() = _isFormValid

    fun notifyIsFormValid(value: Boolean) {
        this._isFormValid.value = value
    }

    // Defined if happens errors to network, if mHasErrors == true, can show components to render message error descriptive to users,
    // for example SnackBar, custom view, toast
    private val _hasErrors = MutableLiveData<Event<Boolean>>()
    val hasErrors: LiveData<Event<Boolean>>
        get() = _hasErrors

    // Defined way to show error
    var renderModeError = RenderModeError.DEFAULT

    // Contain description error
    private var _error = MutableLiveData<GenericAPIError>()
    val error: LiveData<GenericAPIError>?
        get() = _error

    // If RenderModeError is default, error is required and overrideMessage optional
    // else overrideMessage is required and error is optional
    fun notifyWhenHasError(
        error: GenericAPIError? = null,
        renderMode: RenderModeError = RenderModeError.DEFAULT,
        overrideMessage: String? = null
    ) {
        this.renderModeError = renderMode
        if (renderMode == RenderModeError.LIST) {
            this._error.value = GenericAPIError(
                ErrorCode.GENERIC_LIST_ERROR,
                overrideMessage ?: "",
                ErrorCode.GENERIC_LIST_ERROR.code
            )
        } else {
            this._error.value = error
        }
        this._hasErrors.value = Event(true)
    }

    // override for observables model
    open fun validateForm(criterion: Boolean) {}
}