package com.example.punkapp.ui.customViews

import android.annotation.SuppressLint
import android.content.Context
import android.content.res.TypedArray
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.databinding.BindingAdapter
import androidx.lifecycle.LifecycleOwner
import androidx.recyclerview.widget.RecyclerView
import com.example.punkapp.R
import com.example.punkapp.common.services.ResultReactiveData
import com.example.punkapp.databinding.CustomListViewBinding

class ListView constructor(
    context: Context,
    attrs: AttributeSet?,
    defStyle: Int
) : ConstraintLayout(context, attrs, defStyle) {

    private lateinit var mBinding: CustomListViewBinding

    constructor(context: Context) : this(context, null, 0)

    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0) {
        initViews(context, attrs, 0)
    }

    @SuppressLint("ResourceAsColor")
    private fun initViews(context: Context, attrs: AttributeSet?, defStyle: Int) {
        val layoutInflater =
            context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        mBinding = CustomListViewBinding.inflate(layoutInflater, this, true)
        lifeCycleOwner?.let {
            mBinding.lifecycleOwner = it
        }
        bindViews(attrs, defStyle)
        setDefaultValues()
    }

    private fun setDefaultValues() {
    }

    private fun bindViews(attrs: AttributeSet?, defStyle: Int) {

        if (!isInEditMode) {
            val typedArray: TypedArray =
                context.obtainStyledAttributes(attrs, R.styleable.ListView, defStyle, 0)
            typedArray.recycle()
        }
    }

    // Base error passing parameters
    var errorTitleIn: String? = null
        set(value) {
            field = value
            mBinding.errorTitle = value
        }

    var errorIsShowTitleIn: Boolean? = null
        set(value) {
            field = value
            mBinding.errorIsShowTitle = value
        }

    var errorMessageIn: String? = null
        set(value) {
            field = value
            mBinding.errorMessage = value
        }

    // si se quiere mostrar un mensaje cuando no haiga datos sin ocupar el buscador, ni la pantalla de error
    var emptyMessageNoDataIn: String? = null
        set(value) {
            field = value
            mBinding.emptyMessageNoData = value
        }

    var emptyIconNoDataIn: Drawable? = null
        set(value) {
            field = value
            mBinding.emptyIconNoData = value
        }

    var errorIsShowMessageIn: Boolean? = null
        set(value) {
            field = value
            mBinding.errorIsShowMessage = value
        }

    var errorIconIn: Drawable? = null
        set(value) {
            field = value
            mBinding.errorIcon = value
        }

    // Custom parameters in custom Views
    var adapterIn: RecyclerView.Adapter<*>? = null
        set(value) {
            field = value
            mBinding.adapter = value
        }

    var dividerItemOrientationIn: String? = null
        set(value) {
            field = value
            mBinding.dividerItemOrientation = value
        }

    var statusIn: ResultReactiveData.Status? = null
        set(value) {
            field = value
            mBinding.status = value
        }

    private val enabledRefreshingInDefault = true
    var enabledRefreshingIn: Boolean? = null
        set(value) {
            field = value ?: enabledRefreshingInDefault
            mBinding.enabledComponentRefreshing = value ?: enabledRefreshingInDefault
        }

    private var lifeCycleOwner = this.context as? LifecycleOwner

    fun getAdapter() = mBinding.recyclerView.adapter
}

object BaseListAdapters {
    @BindingAdapter(
        value = ["baseList_adapter", "baseList_resultStatus", "baseList_defaultIcon", "baseList_searchText", "baseList_emptyMessageNoData", "baseList_emptyIconNoData"],
        requireAll = false
    )
    @JvmStatic
    fun setVisibilityLayoutsFailure(
        errorView: ErrorView?,
        adapter: RecyclerView.Adapter<*>?,
        resultStatus: ResultReactiveData.Status?,
        defaultIcon: Drawable?,
        searchText: String?,
        emptyMessageNoData: String?,
        emptyIconNoData: Drawable?
    ) {
        if (errorView != null) {
            // check status loading
            if (resultStatus == ResultReactiveData.Status.LOADING) {
                errorView.visibility = View.GONE
                return
            }

            // checks status cause error | empty data
            val itemCount = adapter?.itemCount ?: 0

            val showLayoutError = resultStatus == ResultReactiveData.Status.ERROR
            val criterionStatus = showLayoutError ||
                    resultStatus == ResultReactiveData.Status.SUCCESS_WITHOUT_DATA ||
                    resultStatus == ResultReactiveData.Status.SUCCESS
            val showLayoutEmptyData = itemCount == 0 && criterionStatus
            val newVisibilityView =
                if (showLayoutEmptyData || showLayoutError) View.VISIBLE else View.GONE

            if (newVisibilityView != errorView.visibility) {
                errorView.visibility = newVisibilityView
            }

            // when is in memory search
            errorView.showMessageIn = !showLayoutEmptyData
            if (showLayoutEmptyData) {
                if (itemCount == 0 && searchText.isNullOrEmpty()) {
                    errorView.titleIn =
                        if (resultStatus == ResultReactiveData.Status.SUCCESS_WITHOUT_DATA) emptyMessageNoData else errorView.messageIn
                    errorView.iconIn = emptyIconNoData
                } else {
                    errorView.titleIn = errorView.context.resources.getString(R.string.empty_data)
                    errorView.iconIn = defaultIcon
                }

            }
        }
    }
}