package com.example.punkapp.ui.app.beers

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.example.punkapp.common.components.observables.FormViewModel
import com.example.punkapp.common.services.ResultReactiveData
import com.example.punkapp.models.api.Beer
import com.example.punkapp.repositories.beers.BeersRepository

typealias TBeers = ResultReactiveData<List<Beer>>

class BeerDetailViewModel(application: Application) :
    FormViewModel<BeersRepository>(application) {

    init {
        this.repository =
            BeersRepository(this.dbContext, this.httpManager.beersRemoteSource)
    }

    private val _beerId = MutableLiveData<Long>()
    val beerId: LiveData<Long>
        get() = _beerId

    fun setBeer(id: Long) {
        this._beerId.value = id
    }

    var beers: LiveData<TBeers> =
        Transformations.switchMap(beerId) { id ->
            return@switchMap repository?.findById(id)
        }
    var beer: LiveData<Beer> =
        Transformations.switchMap(beers) { response ->
            if (response?.status == ResultReactiveData.Status.SUCCESS) {
                response.data?.let { beerItems ->
                    return@switchMap MutableLiveData(beerItems.first())
                }
            }
            return@switchMap null
        }
}