package com.example.punkapp.helpers

import android.content.Context
import android.content.res.ColorStateList
import android.graphics.drawable.GradientDrawable
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.annotation.ColorRes
import androidx.core.content.ContextCompat
import androidx.core.view.ViewCompat
import androidx.core.view.updateMargins
import androidx.core.view.updateMarginsRelative
import com.example.punkapp.R
import com.google.android.material.snackbar.Snackbar

enum class DisclaimerType {
    SUCCESS,
    ERROR
}

enum class DisclaimerDuration(val value: Int) {
    INDEFINITE(Snackbar.LENGTH_INDEFINITE),
    SHORT(Snackbar.LENGTH_SHORT),
    LONG(Snackbar.LENGTH_LONG)
}

enum class DisclaimerMode {
    DEFAULT,
    CARD
}

// https://medium.com/@Tgo1014/creating-googles-new-snackbar-b0fe8db6c0eb
class DisclaimerHelper {
    companion object {

        fun showDisclaimer(
            type: DisclaimerType = DisclaimerType.SUCCESS,
            view: View,
            message: String,
            duration: DisclaimerDuration = DisclaimerDuration.SHORT,
            mode: DisclaimerMode = DisclaimerMode.DEFAULT
        ): Snackbar {
            val snackBar =
                Snackbar.make(view, message, duration.value)

            val color = when (type) {
                DisclaimerType.ERROR -> R.color.colorRed
                else -> R.color.colorAccent
            }

            // Override settings
            if (mode == DisclaimerMode.DEFAULT) {
                snackBar.view.setBackgroundColor(
                    ContextCompat.getColor(
                        view.context,
                        color
                    )
                )
            } else if (mode == DisclaimerMode.CARD) {
                setRoundBordersBg(view.context, snackBar, color)
                // BugFix: Interceptor view only works CoordinatorLayout
                (snackBar.view.layoutParams as ViewGroup.MarginLayoutParams).apply {
                    updateMargins(24, 0, 24, 24)
                    updateMarginsRelative(24, 0, 24, 24)
                }

                ViewCompat.setElevation(snackBar.view, 6f)
            }

            snackBar.setActionTextColor(
                ContextCompat.getColor(
                    view.context,
                    R.color.colorWhite
                )
            )

            val snackTextView = snackBar.view.findViewById<View>(R.id.snackbar_text) as TextView
            snackTextView.maxLines = 6
            // Changed size dimension size
            //// snackTextView.textSize = view.context.resources.getDimension(R.dimen.font_size_smaller)

            return snackBar
        }

        private fun setRoundBordersBg(context: Context, snackBar: Snackbar, @ColorRes color: Int) {
            snackBar.view.background = drawableRectangle(context, color)
        }

        private fun drawableRectangle(context: Context, @ColorRes color: Int): GradientDrawable {
            val gd = GradientDrawable()
            gd.shape = GradientDrawable.RECTANGLE
            gd.color = ColorStateList.valueOf(ContextCompat.getColor(context, color))
            gd.cornerRadius = 30.0F
            return gd
        }
    }
}