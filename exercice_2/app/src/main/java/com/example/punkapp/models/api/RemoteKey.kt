package com.example.punkapp.models.api

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "remote_keys")
data class RemoteKeys(
    @PrimaryKey val beerId: Long,
    val prevKey: Int?,
    val nextKey: Int?
)