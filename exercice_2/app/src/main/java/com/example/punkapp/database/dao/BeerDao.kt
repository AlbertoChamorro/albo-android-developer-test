package com.example.punkapp.database.dao

import androidx.paging.PagingSource
import androidx.room.*
import com.example.punkapp.models.api.Beer

// https://medium.com/androiddevelopers/database-relations-with-room-544ab95e4542
// https://developer.android.com/training/data-storage/room/relationships
@Dao
interface BeerDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(beers: List<Beer>)

    @Query("SELECT * FROM beers")
    fun getBeers(): PagingSource<Int, Beer>

    @Query("DELETE FROM beers")
    suspend fun clearBeers()

    @Query("SELECT * FROM beers where id = :id")
    fun findById(id: Long): Beer
}