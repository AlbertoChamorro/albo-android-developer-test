package com.example.punkapp.database.dao

import android.util.Log
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Update
import androidx.room.OnConflictStrategy
import androidx.room.Transaction

@Dao
abstract class BaseDao<TModel> {
    @Delete
    abstract suspend fun delete(model: TModel): Int

    @Insert
    abstract suspend fun add(model: TModel): Long

    @Update
    abstract suspend fun update(model: TModel): Int

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    abstract suspend fun addIgnore(model: TModel): Long

    @Transaction
    open suspend fun addOrUpdate(model: TModel) {
        val id: Long = addIgnore(model)
        Log.d("addOrUpdate", "id = $id")
        if (id == -1L) {
            // si existe lo actualiza
            update(model)
            Log.d("addOrUpdate", "id == -1L")
        }
    }
}