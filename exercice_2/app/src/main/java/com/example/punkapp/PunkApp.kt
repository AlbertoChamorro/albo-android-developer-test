package com.example.punkapp

import android.app.Application

class PunkApp : Application() {

    companion object {
        var shared: PunkApp? = null
    }

    val baseUrl = BuildConfig.BASE_API_URI

    override fun onCreate() {
        super.onCreate()

        if (shared == null) {
            shared = this
        }
    }
}