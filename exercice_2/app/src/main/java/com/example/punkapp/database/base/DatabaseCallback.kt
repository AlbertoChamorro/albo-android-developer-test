package com.example.punkapp.database.base

import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.example.punkapp.database.PunkDatabaseContext
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class DatabaseCallback(
    private val scope: CoroutineScope
) : RoomDatabase.Callback() {

    override fun onOpen(db: SupportSQLiteDatabase) {
        super.onOpen(db)
        PunkDatabaseContext.INSTANCE?.let { database ->
            scope.launch(Dispatchers.IO) {
                 // Seed.populate(context = database)
            }
        }
    }
}