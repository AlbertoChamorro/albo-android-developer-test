package com.example.punkapp.ui.customViews

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import com.example.punkapp.R
import com.example.punkapp.databinding.CustomErrorViewBinding

class ErrorView constructor(
    context: Context,
    attrs: AttributeSet?,
    defStyle: Int
) : ConstraintLayout(context, attrs, defStyle) {

    private lateinit var mBinding: CustomErrorViewBinding

    constructor(context: Context) : this(context, null, 0)

    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0) {
        initViews(context)
    }

    @SuppressLint("ResourceAsColor")
    private fun initViews(context: Context) {
        val layoutInflater =
            context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        mBinding = CustomErrorViewBinding.inflate(layoutInflater, this, true)
        setDefaultValues()
    }

    private fun setDefaultValues() {
    }

    private val titleInDefault = context.resources.getString(R.string.empty_data)
    var titleIn: String? = null
        set(value) {
            field = value ?: titleInDefault
            mBinding.title = value ?: titleInDefault
        }

    private val showTitleInDefault = true
    var showTitleIn: Boolean? = null
        set(value) {
            field = value ?: showTitleInDefault
            mBinding.isShowTitle = value ?: showTitleInDefault
        }

    private val messageInDefault = context.resources.getString(R.string.app_name)
    var messageIn: String? = null
        set(value) {
            field = value ?: messageInDefault
            mBinding.message = value ?: messageInDefault
        }

    private val showMessageInDefault = true
    var showMessageIn: Boolean? = null
        set(value) {
            field = value ?: showMessageInDefault
            mBinding.isShowMessage = value ?: showMessageInDefault
        }

    private val iconInInDefault = ContextCompat.getDrawable(
        context,
        R.drawable.vc_404
    )
    var iconIn: Drawable? = null
        set(value) {
            field = value ?: iconInInDefault
            mBinding.icon = value ?: iconInInDefault
        }
}