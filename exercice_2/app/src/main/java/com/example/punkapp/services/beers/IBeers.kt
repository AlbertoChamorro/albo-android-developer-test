package com.example.punkapp.services.beers

import com.example.punkapp.models.api.Beer
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.QueryMap

/**
 * IBeers REST API access points
 */
interface IBeers {
    @GET("beers")
    suspend fun getAll(@QueryMap filter: Map<String, String>): Response<List<Beer>>

    @GET("beers/{id}")
    suspend fun findById(@Path("id") id: Long): Response<List<Beer>>

}