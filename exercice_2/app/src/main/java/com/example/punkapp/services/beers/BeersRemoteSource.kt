package com.example.punkapp.services.beers

import com.example.punkapp.common.services.PagedQueryString
import com.example.punkapp.models.api.Beer
import com.example.punkapp.services.base.BaseDataSource
import retrofit2.HttpException
import java.io.IOException

/**
 * Works with the API data.
 */

private const val STARTING_PAGE_INDEX = 1

class BeersRemoteSource constructor(val service: IBeers) :
    BaseDataSource<Beer>() {

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, Beer> {
        val position = params.key ?: STARTING_PAGE_INDEX
        return try {
            val filter = PagedQueryString(
                page = position
            )
            val response = service.getAll(this.createPagedQueryString(model = filter))
            val beers = response.body() ?: listOf()
            val nextKey = if (beers.isEmpty()) {
                null
            } else {
                // initial load size = 3 * NETWORK_PAGE_SIZE
                position + 1
            }
            LoadResult.Page(
                data = beers,
                prevKey = if (position == STARTING_PAGE_INDEX) null else position - 1,
                nextKey = nextKey
            )
        } catch (exception: IOException) {
            return LoadResult.Error(exception)
        } catch (exception: HttpException) {
            return LoadResult.Error(exception)
        }
    }

    suspend fun findById(id: Long) =
        getResult { service.findById(id) }
}