package com.example.punkapp.repositories.beers

import androidx.paging.ExperimentalPagingApi
import androidx.paging.LoadType
import androidx.paging.PagingState
import androidx.paging.RemoteMediator
import androidx.room.withTransaction
import com.example.punkapp.common.services.PagedQueryString
import com.example.punkapp.database.PunkDatabaseContext
import com.example.punkapp.helpers.ViewHelper
import com.example.punkapp.models.api.Beer
import com.example.punkapp.models.api.RemoteKeys
import com.example.punkapp.services.beers.IBeers
import retrofit2.HttpException
import java.io.IOException
import java.io.InvalidObjectException

private const val STARTING_PAGE_INDEX = 1

@OptIn(ExperimentalPagingApi::class)
open class BeersRemoteMediator(
    private val service: IBeers,
    private val database: PunkDatabaseContext
) : RemoteMediator<Int, Beer>() {

    override suspend fun load(loadType: LoadType, state: PagingState<Int, Beer>): MediatorResult {

        val page = when (loadType) {
            LoadType.REFRESH -> {
                val remoteKeys = getRemoteKeyClosestToCurrentPosition(state)
                remoteKeys?.nextKey?.minus(1) ?: STARTING_PAGE_INDEX
            }
            LoadType.PREPEND -> {
                val remoteKeys = getRemoteKeyForFirstItem(state)
                    ?: // The LoadType is PREPEND so some data was loaded before,
                    // so we should have been able to get remote keys
                    // If the remoteKeys are null, then we're an invalid state and we have a bug
                    throw InvalidObjectException("Remote key and the prevKey should not be null")
                // If the previous key is null, then we can't request more data
                val prevKey = remoteKeys.prevKey ?: return MediatorResult.Success(endOfPaginationReached = true)
                prevKey
            }
            LoadType.APPEND -> {
                val remoteKeys = getRemoteKeyForLastItem(state)
                if (remoteKeys?.nextKey == null) {
                    throw InvalidObjectException("Remote key should not be null for $loadType")
                }
                remoteKeys.nextKey
            }

        }

        try {
            val filter = PagedQueryString(
                page = page,
                pageSize = state.config.pageSize
            )
            val response = service.getAll(this.createPagedQueryString(model = filter))
            val beers = response.body() ?: listOf()
            val endOfPaginationReached = beers.isEmpty()
            database.withTransaction {
                // clear all tables in the database
                if (loadType == LoadType.REFRESH) {
                    database.remoteKeyDao().clearRemoteKeys()
                    database.beerDao().clearBeers()
                }
                val prevKey = if (page == STARTING_PAGE_INDEX) null else page - 1
                val nextKey = if (endOfPaginationReached) null else page + 1
                val keys = beers.map {
                    RemoteKeys(beerId = it.id, prevKey = prevKey, nextKey = nextKey)
                }
                database.remoteKeyDao().insertAll(keys)
                database.beerDao().insertAll(beers)
            }
            return MediatorResult.Success(endOfPaginationReached = endOfPaginationReached)
        } catch (exception: IOException) {
            return MediatorResult.Error(exception)
        } catch (exception: HttpException) {
            return MediatorResult.Error(exception)
        }
    }

    private suspend fun getRemoteKeyForLastItem(state: PagingState<Int, Beer>): RemoteKeys? {
        // Get the last page that was retrieved, that contained items.
        // From that last page, get the last item
        return state.pages.lastOrNull { it.data.isNotEmpty() }?.data?.lastOrNull()
            ?.let { beer ->
                // Get the remote keys of the last item retrieved
                database.remoteKeyDao().remoteKeysBeerId(beer.id)
            }
    }

    private suspend fun getRemoteKeyForFirstItem(state: PagingState<Int, Beer>): RemoteKeys? {
        // Get the first page that was retrieved, that contained items.
        // From that first page, get the first item
        return state.pages.firstOrNull { it.data.isNotEmpty() }?.data?.firstOrNull()
            ?.let { beer ->
                // Get the remote keys of the first items retrieved
                database.remoteKeyDao().remoteKeysBeerId(beer.id)
            }
    }

    private suspend fun getRemoteKeyClosestToCurrentPosition(
        state: PagingState<Int, Beer>
    ): RemoteKeys? {
        // The paging library is trying to load data after the anchor position
        // Get the item closest to the anchor position
        return state.anchorPosition?.let { position ->
            state.closestItemToPosition(position)?.id?.let { beerId ->
                database.remoteKeyDao().remoteKeysBeerId(beerId)
            }
        }
    }

    private fun createPagedQueryString(model: PagedQueryString): MutableMap<String, String> {
        return ViewHelper.createPagedQueryString(model)
    }
}