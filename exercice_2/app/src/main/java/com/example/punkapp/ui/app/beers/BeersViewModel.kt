package com.example.punkapp.ui.app.beers

import android.app.Application
import androidx.lifecycle.viewModelScope
import androidx.paging.ExperimentalPagingApi
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.example.punkapp.common.components.emitters.SingleLiveEvent
import com.example.punkapp.common.components.observables.FormViewModel
import com.example.punkapp.common.services.PagedQueryString
import com.example.punkapp.models.api.Beer
import com.example.punkapp.repositories.beers.BeersRepository
import kotlinx.coroutines.flow.Flow

typealias TPagedBeers = PagingData<Beer>

class BeersViewModel(application: Application) :
    FormViewModel<BeersRepository>(application) {

    init {
        this.repository =
            BeersRepository(this.dbContext, this.httpManager.beersRemoteSource)
    }

    // Events
    val beerIdArgDestination = SingleLiveEvent<Long>()

    fun navigateToShowDetail(binnacleId: Long) {
        beerIdArgDestination.value = binnacleId
    }

    var filters = PagedQueryString()

    var currentSearchResult: Flow<TPagedBeers>? = null

    @ExperimentalPagingApi
    fun getBeers(): Flow<PagingData<Beer>> {
        val newResult: Flow<TPagedBeers> = repository!!.getBeers(filters)
            .cachedIn(viewModelScope)
        currentSearchResult = newResult
        return newResult
    }
}