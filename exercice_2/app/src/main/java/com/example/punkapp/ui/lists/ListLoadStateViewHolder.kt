package com.example.punkapp.ui.lists

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.paging.LoadState
import androidx.recyclerview.widget.RecyclerView
import com.example.punkapp.R
import com.example.punkapp.databinding.ListLoaderFooterViewBinding

class ListLoadStateViewHolder(
    private val binding: ListLoaderFooterViewBinding,
    retry: () -> Unit
) : RecyclerView.ViewHolder(binding.root) {

    init {
        binding.retryButton.setOnClickListener { retry.invoke() }
    }

    fun bind(loadState: LoadState) {
        if (loadState is LoadState.Error) {
            // binding.errorMsg.text = loadState.error.localizedMessage
        }
        binding.baseLoadingView.isVisible = loadState is LoadState.Loading
        binding.retryButton.isVisible = loadState !is LoadState.Loading
        // binding.errorMsg.isVisible = loadState !is LoadState.Loading
    }

    companion object {
        fun create(parent: ViewGroup, retry: () -> Unit): ListLoadStateViewHolder {
            val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.list_loader_footer_view, parent, false)
            val binding = ListLoaderFooterViewBinding.bind(view)
            return ListLoadStateViewHolder(binding, retry)
        }
    }
}