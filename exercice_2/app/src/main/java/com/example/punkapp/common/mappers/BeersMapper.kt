package com.example.punkapp.common.mappers

import com.example.punkapp.common.services.PagedQueryString
import com.example.punkapp.common.services.PagedResponse

object BeersMapper {
    fun <T> List<T>.asPaginateModel(queries: PagedQueryString): PagedResponse<T> {
        return PagedResponse(
            records = this,
            pageSize = queries.pageSize,
            currentPage = queries.page,
            count = this.size
        )
    }
}