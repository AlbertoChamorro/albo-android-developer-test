package com.example.punkapp.ui.app

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.punkapp.common.components.emitters.Event
import com.example.punkapp.common.components.observables.ObservableViewModel

class MainViewModel(application: Application) : ObservableViewModel(application) {

    // Defined toolbar UI to show/hidden component
    private val mTitle = MutableLiveData<String>()
    val title: LiveData<String>
        get() = mTitle

    private val mSubtitle = MutableLiveData<String>()
    val subtitle: LiveData<String>
        get() = mSubtitle

    private val mShowBackButton = MutableLiveData<Boolean>()
    val showBackButton: LiveData<Boolean>
        get() = mShowBackButton

    private val mIsShowToolbar = MutableLiveData<Boolean>()
    val isShowToolbar: LiveData<Boolean>
        get() = mIsShowToolbar

    private val mLastChangeNotified = MutableLiveData<Event<String>>()
    val lastChangeNotified: LiveData<Event<String>>
        get() = mLastChangeNotified

    private val mEnableDrawerMenu = MutableLiveData<Boolean>(false)
    val enableDrawerMenu: LiveData<Boolean>
        get() = mEnableDrawerMenu

    fun setEnableDrawerMenu(value: Boolean) {
        mEnableDrawerMenu.value = value
    }

    fun showToolbar(state: Boolean) {
        mIsShowToolbar.value = state
    }

    fun notifyChangeFrom(featureKey: String) {
        mLastChangeNotified.value = Event(featureKey)
    }

    fun setTitle(title: String?) {
        mTitle.value = title
    }

    fun setSubtitle(subtitle: String?) {
        mSubtitle.value = subtitle
    }

    fun showBackButton(showBackButton: Boolean) {
        mShowBackButton.value = showBackButton
    }
}
