package com.example.punkapp.database.converters

import androidx.room.TypeConverter

class RoomConverters {

    /// List Strings
    @TypeConverter
    fun stringToList(value: String): List<String>? = value.split(",")

    @TypeConverter
    fun listToString(list: List<String>): String = list.joinToString(separator = ",")

}