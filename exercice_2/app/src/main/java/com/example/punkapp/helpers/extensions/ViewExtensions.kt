package com.example.punkapp.helpers.extensions

import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.example.punkapp.helpers.ViewHelper

fun Fragment.dismissKeyboard() {
    val activity = this.activity
    if (activity is AppCompatActivity) {
        ViewHelper.dismissKeyboard(activity)
    }
}

fun Fragment.dismissKeyboard(view: View) {
    val activity = this.activity
    if (activity is AppCompatActivity) {
        ViewHelper.dismissKeyboard(activity, view)
    }
}

@Suppress("unused")
fun AppCompatActivity.dismissKeyboard(view: View) {
    ViewHelper.dismissKeyboard(this, view)
}