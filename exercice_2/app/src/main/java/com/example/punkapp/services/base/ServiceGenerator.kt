package com.example.punkapp.services.base

import com.example.punkapp.PunkApp
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

/*
    Dynamic uri
    https://futurestud.io/tutorials/retrofit-2-how-to-use-dynamic-urls-for-requests

    Configure retrofit options
    https://futurestud.io/tutorials/retrofit-2-how-to-change-api-base-url-at-runtime-2

    Added Headers intercept
    https://github.com/codepath/android_guides/wiki/Consuming-APIs-with-Retrofit
 */

class ServiceGenerator {

    private var _baseUrl: String = PunkApp.shared?.baseUrl ?: ""
        get() {
            return "${field}/v2/"
        }

    private var builder: Retrofit.Builder = getInstanceRetrofitBuilder()

    private var retrofit = builder.build()

    private val httpClient = getClientBuilder()

    private val httpInterceptor: HttpLoggingInterceptor = HttpLoggingInterceptor().apply {
        this.level = HttpLoggingInterceptor.Level.BODY
    }

    private fun getInstanceRetrofitBuilder(): Retrofit.Builder {
        return Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
            .baseUrl(_baseUrl)
    }

    private fun getClientBuilder(): OkHttpClient.Builder {

        // All timeout is 10 seconds by default
        return OkHttpClient.Builder()
            .connectTimeout(1, TimeUnit.MINUTES)
            .readTimeout(30, TimeUnit.SECONDS)
            .writeTimeout(15, TimeUnit.SECONDS)
    }

    fun <TService> createService(
        serviceClass: Class<TService>
    ): TService {
        if (!httpClient.networkInterceptors().contains(httpInterceptor)) {
            httpClient.networkInterceptors().add(httpInterceptor)

            builder.client(httpClient.build())
            retrofit = builder.build()
        }

        return retrofit.create<TService>(serviceClass)
    }
}