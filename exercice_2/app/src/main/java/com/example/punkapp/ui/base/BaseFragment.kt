package com.example.punkapp.ui.base

import android.content.res.Resources
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.activity.OnBackPressedCallback
import androidx.annotation.ColorInt
import androidx.annotation.ColorRes
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import com.example.punkapp.PunkApp
import com.example.punkapp.R
import com.example.punkapp.common.components.emitters.EventObserver
import com.example.punkapp.common.components.observables.BaseObservableViewModel
import com.example.punkapp.common.components.observables.FormViewModel
import com.example.punkapp.common.components.observables.RenderModeError
import com.example.punkapp.helpers.DisclaimerDuration
import com.example.punkapp.helpers.DisclaimerHelper
import com.example.punkapp.helpers.DisclaimerMode
import com.example.punkapp.helpers.DisclaimerType
import com.example.punkapp.helpers.extensions.dismissKeyboard
import com.example.punkapp.ui.app.MainActivity

abstract class BaseFragment : Fragment() {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        this.setStatusBarColor(this.getColor(R.color.colorPrimaryDark))
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        // Configured common styles for screen
        this.setupNavHost()
    }

    val baseApplication: PunkApp by lazy {
        PunkApp.shared?.let { app -> return@let app }
        return@lazy PunkApp()
    }

    var onBackPressedHandle: (() -> Unit)? = null
        set(value) {
            field = value
            requireActivity().onBackPressedDispatcher.addCallback(
                viewLifecycleOwner,
                onBackPressedCallback
            )
        }

    private val onBackPressedCallback = object : OnBackPressedCallback(true) {
        override fun handleOnBackPressed() {
            onBackPressedHandle?.invoke()
        }
    }

    protected fun getColor(@ColorRes color: Int): Int {
        context?.let { cnx ->
            return ContextCompat.getColor(
                cnx,
                color
            )
        }
        return -1
    }

    protected fun setStatusBarColor(@ColorInt color: Int) {
        val parent = activity as? BaseActivity
        parent?.setStatusBarColor(color)
    }

    private fun setSupportActionBar(toolbar: Toolbar) {
        this.baseActivity.setSupportActionBar(toolbar)
    }

    private fun showDisclaimer(
        type: DisclaimerType = DisclaimerType.SUCCESS,
        view: View? = null,
        message: String,
        duration: DisclaimerDuration = DisclaimerDuration.SHORT
    ) {
        DisclaimerHelper.showDisclaimer(
            type = type,
            view = view ?: this.baseActivity.rootView,
            message = message,
            duration = duration,
            mode = DisclaimerMode.CARD
        ).show()
    }

    protected val baseActivity: MainActivity by lazy {
        this.activity.let { act ->
            act as MainActivity
        }
    }

    fun <T : Any> onSubscribeReactiveFormData(formViewModel: FormViewModel<T>) {
        this.onSubscribeBaseObservableData(formViewModel)

        formViewModel.hasErrors.observe(viewLifecycleOwner, EventObserver { value ->
            if (value) {
                if (formViewModel.renderModeError == RenderModeError.DEFAULT) {
                    this.notifyError(
                        message = formViewModel.error?.value?.message ?: ""
                    )
                }
                // ToDo: implemented other types error in view, in activity main layout xml or fragment main layout
            }
        })
    }

    private fun <T : Any> onSubscribeBaseObservableData(baseViewModel: BaseObservableViewModel<T>) {
        baseViewModel.dismissKeyBoardEvent.observe(viewLifecycleOwner, Observer { value ->
            if (value) this.dismissKeyboard()
        })
    }

    fun notifySuccess(message: String, view: View? = null) {
        this.showDisclaimer(
            type = DisclaimerType.SUCCESS,
            message = message,
            view = view,
            duration = DisclaimerDuration.LONG
        )
    }

    fun notifyError(message: String) {
        this.showDisclaimer(
            type = DisclaimerType.ERROR,
            message = message,
            duration = DisclaimerDuration.LONG
        )
    }

    fun findRootNavController(): NavController? {
        return this.parentFragment?.parentFragment?.findNavController()
    }

    private fun setupNavHost() {
        this.baseActivity.let { activity ->
            if (activity.toolbar == null) {
                activity.toolbar = activity.findViewById(R.id.mainToolbar)
            }

            if (activity.appBarConfiguration == null) {
                val host: NavHostFragment = activity.supportFragmentManager
                    .findFragmentById(R.id.main_host_fragment) as NavHostFragment? ?: return

                // Set up Action Bar
                val navController = host.navController
                activity.appBarConfiguration = AppBarConfiguration(
                    navController.graph
                )
                // , setOf(R.id.homeFragment, R.id.homeFragment2)
                activity.appBarConfiguration?.let {
                    setupActionBar(navController, it)
                }

                navController.addOnDestinationChangedListener { _, destination, _ ->
                    val dest: String = try {
                        baseActivity.resources.getResourceName(destination.id)
                    } catch (e: Resources.NotFoundException) {
                        destination.id.toString()
                    }

                    Log.d(
                        BaseFragment::class.java.simpleName,
                        "Navigated to $dest"
                    )
                }
            }
        }
    }

    private fun setupActionBar(
        navController: NavController,
        appBarConfig: AppBarConfiguration
    ) {
        // This allows NavigationUI to decide what label to show in the action bar by using appBarConfig, it will also determine whether to
        // show the up arrow or drawer menu icon
        this.baseActivity.setupActionBarWithNavController(navController, appBarConfig)
    }
}
