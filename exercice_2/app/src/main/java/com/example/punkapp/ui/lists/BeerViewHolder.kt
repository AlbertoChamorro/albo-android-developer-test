package com.example.punkapp.ui.lists

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.punkapp.R
import com.example.punkapp.databinding.HolderBeerViewBinding
import com.example.punkapp.models.api.Beer

/**
 * class [BeerViewHolder].
 */
class BeerViewHolder(private val binding: HolderBeerViewBinding) :
    RecyclerView.ViewHolder(binding.root) {
    fun bind(
        item: Beer,
        onSelectItem: () -> Unit
    ) {
        binding.beer = item
        binding.showDetail = false
        binding.setOnClick { onSelectItem.invoke() }
        binding.executePendingBindings()
    }

    companion object {
        fun create(parent: ViewGroup): BeerViewHolder {
            return BeerViewHolder(
                DataBindingUtil.inflate(
                    LayoutInflater.from(parent.context),
                    R.layout.holder_beer_view,
                    parent,
                    false
                )
            )
        }
    }
}