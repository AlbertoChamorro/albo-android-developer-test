package com.example.punkapp.ui.lists

import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import com.example.punkapp.models.api.Beer
import com.example.punkapp.ui.app.beers.BeersViewModel

class BeersAdapter(private val beersViewModel: BeersViewModel) :
    PagingDataAdapter<Beer, BeerViewHolder>(BEER_COMPARATOR) {

    override fun onBindViewHolder(holder: BeerViewHolder, position: Int) {
        val beer = getItem(position)
        if (beer != null) {
            holder.bind(beer) { beersViewModel.navigateToShowDetail(beer.id) }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BeerViewHolder {
        return BeerViewHolder.create(parent)
    }

    companion object {
        private val BEER_COMPARATOR = object : DiffUtil.ItemCallback<Beer>() {
            override fun areItemsTheSame(oldItem: Beer, newItem: Beer): Boolean =
                oldItem.id == newItem.id

            override fun areContentsTheSame(
                oldItem: Beer,
                newItem: Beer
            ): Boolean =
                oldItem == newItem
        }
    }
}