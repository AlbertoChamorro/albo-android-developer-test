package com.example.punkapp.services.base

abstract class BaseService {
    protected val httpManager: BaseHttpManager by lazy { BaseHttpManager.shared }
}