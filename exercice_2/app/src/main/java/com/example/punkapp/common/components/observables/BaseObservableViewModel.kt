package com.example.punkapp.common.components.observables

import android.app.Application
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import com.example.punkapp.common.components.emitters.SingleLiveEvent
import com.example.punkapp.database.PunkDatabaseContext
import com.example.punkapp.services.base.BaseHttpManager
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.disposables.Disposable

open class BaseObservableViewModel<TRepository>(application: Application) :
    ObservableViewModel(application)
        where TRepository : Any {

    // Http manager remotes services web api
    protected val httpManager: BaseHttpManager by lazy { BaseHttpManager.shared }

    // Defined loading UI to show/hidden component
    val isLoadingEvent = SingleLiveEvent<Boolean>()

    // Defined dismiss keyboard
    val dismissKeyBoardEvent = SingleLiveEvent<Boolean>()

    // Manager data sources database local
    protected var repository: TRepository? = null

    /* Handle context application database */
    protected var dbContext: PunkDatabaseContext =
        PunkDatabaseContext.getInstance(application, viewModelScope)

    /* Handled queue historic https observers */
    private var mCompositeDisposable: CompositeDisposable? = CompositeDisposable()

    protected var savedStateHandle: SavedStateHandle? = null

    // override for logic cleaner bundle
    open fun clearBundleSavedState() {}

    fun registerSavedStateBackStackEntry(savedStateHandle: SavedStateHandle?) {
        this.savedStateHandle = savedStateHandle
    }

    protected fun addOperation(operation: Disposable) {
        this.mCompositeDisposable?.add(operation)
    }

    /* Lifecycle removed all queue on destroy observer */
    override fun onCleared() {
        super.onCleared()
        this.removeOperation()
    }

    /* Removed observer listeners https */
    private fun removeOperation() {
        this.mCompositeDisposable?.clear()
    }

    fun onCleared(force: Boolean) {
        if (force) {
            this.onCleared()
        }
    }
}
