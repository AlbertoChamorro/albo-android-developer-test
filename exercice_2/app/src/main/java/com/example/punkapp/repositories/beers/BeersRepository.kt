package com.example.punkapp.repositories.beers

import androidx.lifecycle.MutableLiveData
import androidx.paging.ExperimentalPagingApi
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import com.example.punkapp.common.services.PagedQueryString
import com.example.punkapp.common.services.ResultReactiveData
import com.example.punkapp.database.PunkDatabaseContext
import com.example.punkapp.models.api.Beer
import com.example.punkapp.repositories.base.BaseRepository
import com.example.punkapp.services.beers.BeersRemoteSource
import kotlinx.coroutines.flow.Flow

typealias TModelBeerDetail = List<Beer>

class BeersRepository(
    private val database: PunkDatabaseContext,
    private val remoteSource: BeersRemoteSource
) :
    BaseRepository() {

    fun findById(id: Long) =
        resultLiveData<TModelBeerDetail, TModelBeerDetail>(
            networkCall = { remoteSource.findById(id) },
            databaseQuery = {
                val item = database.beerDao().findById(id)
                return@resultLiveData MutableLiveData(ResultReactiveData.success(listOf(item)))
            }
        )

    @ExperimentalPagingApi
    fun getBeers(filter: PagedQueryString): Flow<PagingData<Beer>> {
        val pagingSourceFactory =
            { database.beerDao().getBeers() }// remoteSource  | database.beerDao.getBeers()
        return Pager(
            config = PagingConfig(pageSize = filter.pageSize, enablePlaceholders = false),
            remoteMediator = BeersRemoteMediator(
                this.remoteSource.service,
                this.database
            ),
            pagingSourceFactory = pagingSourceFactory
        ).flow
    }
}