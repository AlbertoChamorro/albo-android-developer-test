package com.example.punkapp.common.components

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.liveData
import com.example.punkapp.common.services.ResultReactiveData
import kotlinx.coroutines.Dispatchers

@Suppress("UNCHECKED_CAST")
class SingleSourceOfTruthStrategy {
    companion object {

        // https://developer.android.com/jetpack/guide
        /**
         * The database serves as the single source of truth.
         * Therefore UI can receive data updates from database only.
         * Function notify UI about:
         * [ResultReactiveData.Status.SUCCESS] - with data from database
         * [ResultReactiveData.Status.ERROR] - if error has occurred from any source
         * [ResultReactiveData.Status.LOADING]
         */

        fun <TSource, TNet> resultLiveData(
            databaseQuery: (suspend () -> LiveData<ResultReactiveData<TSource>>)? = null,
            networkCall: (suspend () -> ResultReactiveData<TNet>)?,
            saveCallResult: (suspend (TNet?) -> Unit)? = null
        ): LiveData<ResultReactiveData<TSource>> = liveData(Dispatchers.IO) {

            var source: LiveData<ResultReactiveData<TSource>>? = null

            // Emitted state is Loading to thread UI
            emit(ResultReactiveData.loading())

            // Model offline is active
            if (databaseQuery != null) {
                // source = databaseQuery.invoke().map {  ResultReactiveData.success(it) }
                source = databaseQuery.invoke()
                emitSource(source)
            }

            networkCall?.let {
                // Only remotes queries, or mode offline if disabled for feature
                val responseStatus = networkCall.invoke()
                when (responseStatus.status) {
                    ResultReactiveData.Status.SUCCESS -> {
                        saveCallResult?.invoke(responseStatus.data)
                        //if (databaseQuery == null) {
                        val responseData = responseStatus.data as TSource
                        // If only records is empty response with status `SUCCESS_WITHOUT_DATA`
                        var successStatus = ResultReactiveData.Status.SUCCESS
                        if (responseData is List<*> &&
                            (responseData as List<*>).size == 0
                        ) {
                            successStatus = ResultReactiveData.Status.SUCCESS_WITHOUT_DATA
                        }
                        source = MutableLiveData(
                            ResultReactiveData(
                                successStatus,
                                responseData,
                                null
                            )
                        )
                        emitSource(source as MutableLiveData<ResultReactiveData<TSource>>)
                        //}
                    }
                    ResultReactiveData.Status.ERROR -> {
                        emit(ResultReactiveData.error(responseStatus.error))
                        source?.let { emitSource(it) }
                    }
                    else -> {
                    }
                }
            }
        }
    }
}