package com.example.punkapp.ui.base

import android.os.Build
import android.os.Bundle
import android.view.KeyEvent
import android.view.View
import android.view.WindowManager
import androidx.annotation.ColorInt
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.navigation.ui.AppBarConfiguration
import com.example.punkapp.R

abstract class BaseActivity : AppCompatActivity() {

    // UI Components commons
    var appBarConfiguration: AppBarConfiguration? = null

    var toolbar: Toolbar? = null

    var title: String = ""

    lateinit var rootView: View

    var onKeyDownFragment: ((keyCode: Int, event: KeyEvent?) -> Boolean)? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        this.setTheme(R.style.AppTheme)
        super.onCreate(savedInstanceState)
    }

    fun setStatusBarColor(@ColorInt color: Int) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val window = window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.statusBarColor = color
        }
    }
}