package com.example.punkapp.ui.lists

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.punkapp.R
import com.example.punkapp.databinding.HolderFoodPairingViewBinding

/**
 * class [FoodPairingAdapter].
 */
open class FoodPairingAdapter(
    private val items: List<String>
) : RecyclerView.Adapter<FoodPairingAdapter.FoodPairingViewHolder>() {

    override fun onBindViewHolder(holder: FoodPairingViewHolder, position: Int) {
        val value = items[position]
        holder.bind(value)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FoodPairingViewHolder {
        return FoodPairingViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                R.layout.holder_food_pairing_view,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return items.size
    }

    /**
     * class [FoodPairingViewHolder].
     */
    inner class FoodPairingViewHolder(private val binding: HolderFoodPairingViewBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(
            value: String
        ) {
            binding.title = value
            binding.executePendingBindings()
        }
    }
}