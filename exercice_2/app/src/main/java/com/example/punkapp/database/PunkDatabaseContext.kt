package com.example.punkapp.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.example.punkapp.BuildConfig
import com.example.punkapp.database.base.DatabaseCallback
import com.example.punkapp.database.converters.RoomConverters
import com.example.punkapp.database.dao.BeerDao
import com.example.punkapp.database.dao.RemoteKeysDao
import com.example.punkapp.models.api.Beer
import com.example.punkapp.models.api.RemoteKeys
import kotlinx.coroutines.CoroutineScope

/*
    Database information debugger
        https://github.com/amitshekhariitbhu/Android-Debug-Database
        https://developers.google.com/web/tools/chrome-devtools/remote-debugging/local-server

    Or use program desktop mac/window
        DB Browser for SQLite
*/

// TODO: make migrations strategies
// https://developer.android.com/training/data-storage/room/migrating-db-versions
@Database(
    entities = [
        Beer::class,
        RemoteKeys::class
    ],
    exportSchema = false,
    version = BuildConfig.VERSION_CODE
)
@TypeConverters(RoomConverters::class)
abstract class PunkDatabaseContext : RoomDatabase() {

    // Abstracts Dao interfaces
    abstract fun beerDao(): BeerDao
    abstract fun remoteKeyDao(): RemoteKeysDao

    companion object {

        @Volatile
        var INSTANCE: PunkDatabaseContext? = null

        var DB_NAME = "DATABASE:${BuildConfig.APPLICATION_ID}"

        fun getInstance(context: Context, scope: CoroutineScope): PunkDatabaseContext =
            INSTANCE ?: synchronized(this) {
                INSTANCE
                    ?: buildDatabase(context, scope).also { INSTANCE = it }
            }

        private fun buildDatabase(context: Context, scope: CoroutineScope) =
            Room.databaseBuilder(
                context.applicationContext,
                PunkDatabaseContext::class.java, DB_NAME
            )
                .addCallback(
                    DatabaseCallback(
                        scope
                    )
                )
                .build()
    }
}