package com.example.punkapp.helpers.extensions

import com.example.punkapp.helpers.ReflectorHelper
import kotlin.reflect.KClass

fun <T : Enum<*>> KClass<T>.fromValue(value: Int, propertyName: String = "code"): T? {
    for (item in this.java.enumConstants!!) {
        val code = ReflectorHelper.readInstanceProperty<Int>(item, propertyName)
        if (code == value) return item
    }

    return null
}
