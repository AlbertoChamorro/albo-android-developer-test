package com.example.punkapp.ui.app

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import com.example.punkapp.R
import com.example.punkapp.common.services.ResultReactiveData
import com.example.punkapp.databinding.FragmentBeerDetailBinding
import com.example.punkapp.ui.app.beers.BeerDetailViewModel
import com.example.punkapp.ui.base.BaseFragment
import com.example.punkapp.ui.lists.FoodPairingAdapter

/**
 * [BeerDetailFragment] subclass of [BaseFragment].
 */
class BeerDetailFragment : BaseFragment() {

    private lateinit var binding: FragmentBeerDetailBinding
    private val args: BeerDetailFragmentArgs by navArgs()
    private val beerDetailViewModel: BeerDetailViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_beer_detail, container, false)
        binding.viewModel = beerDetailViewModel
        binding.lifecycleOwner = this
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        this.initViews()
    }

    private fun initViews() {
        // set Title app
        baseActivity.mainViewModel.setTitle(resources.getString(R.string.loadOperator))

        // get args from safe args, if screen is detail action
        if (beerDetailViewModel.beerId.value == null) {
            beerDetailViewModel.setBeer(args.id)
        }

        beerDetailViewModel.beers.observe(
            viewLifecycleOwner,
            Observer { result ->
                when (result.status) {
                    ResultReactiveData.Status.SUCCESS -> {
                        beerDetailViewModel.isLoadingEvent.value = false
                    }
                    ResultReactiveData.Status.LOADING -> {
                        beerDetailViewModel.isLoadingEvent.value = true
                    }
                    else -> {
                        // ERROR
                        beerDetailViewModel.isLoadingEvent.value = false
                        beerDetailViewModel.notifyWhenHasError(error = result.error)
                    }
                }
            })

        beerDetailViewModel.beer.observe(
            viewLifecycleOwner,
            Observer { result ->
                if (result != null) {
                    baseActivity.mainViewModel.setTitle(result.name)
                    result.foodPairing?.let {
                        binding.adapter = FoodPairingAdapter(it)
                    }
                }
            })
    }
}