package com.example.punkapp.services.base

import androidx.paging.PagingSource
import com.example.punkapp.PunkApp
import com.example.punkapp.common.services.GenericAPIError
import com.example.punkapp.common.services.PagedQueryString
import com.example.punkapp.common.services.ResultReactiveData
import com.example.punkapp.helpers.ViewHelper
import retrofit2.Response

/**
 * Abstract Base Data source class with error handling
 */
abstract class BaseDataSource<T : Any> : PagingSource<Int, T>() {
    protected suspend fun <T> getResult(call: suspend () -> Response<T>): ResultReactiveData<T> {
        try {
            val response = call()
            if (response.isSuccessful) {
                val body = response.body()
                if (body != null) return ResultReactiveData.success(body)
            }
            return error(
                ErrorUtils.onHandleError(
                    response = response,
                    context = PunkApp.shared?.baseContext!!
                )
            )
        } catch (e: Exception) {
            return error(
                ErrorUtils.onHandleError(
                    exception = e,
                    context = PunkApp.shared?.baseContext!!
                )
            )
        }
    }

    private fun <T> error(exceptionApi: GenericAPIError?): ResultReactiveData<T> {
        return ResultReactiveData.error(exceptionApi)
    }

    protected fun createPagedQueryString(model: PagedQueryString): MutableMap<String, String> {
        return ViewHelper.createPagedQueryString(model)
    }
}