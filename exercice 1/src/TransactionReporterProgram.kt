import com.google.gson.Gson
import okhttp3.*
import java.io.IOException

fun main(args: Array<String>) {
    val url =
        "https://gist.githubusercontent.com/astrocumbia/06ec83050ec79170b10a11d1d4924dfe/raw/ad791cddcff6df2ec424bfa3da7cdb86f266c57e/transactions.json"
    getTransactions(url)
}

private fun getTransactions(url: String) {
    val request = Request.Builder()
        .url(url)
        .build()

    val client = OkHttpClient()
    client.newCall(request).enqueue(object : Callback {
        override fun onFailure(call: Call, e: IOException) {
            println("Something has happen bad :<")
        }

        override fun onResponse(call: Call, response: Response) {
            val responseData = "{" + "data:" + response.body()!!.string() + "}"
            val library = Gson()
            val data = library.fromJson(responseData, ApiResponse::class.java)
            printTransactionReporterByMonth(data)
        }
    })
}

private fun printTransactionReporterByMonth(data: ApiResponse) {
    val groupData = data.data?.groupByMonth()
    println("--------------------------------------------------------------------")
    groupData?.forEach { item ->
        println(item.month)
        println("\t${item.pendingTransactions} transacciones pendientes")
        println("\t${item.blockedTransaction} bloqueadas")
        println("\n\t$${item.incomes.toAmount()} ingresos")
        println("\n\t$${item.expenses.toAmount()} gastos")
        println("")
        println("\tCATEGORIAS")
        item.categories.forEach { category ->
            println("\t\t${category.name} %${category.percentage.toAmount()}")
        }

        println("--------------------------------------------------------------------")
    }
}
