import java.math.RoundingMode
import java.text.DateFormatSymbols
import java.text.DecimalFormat
import java.util.*

class Helper {
    companion object {
        fun getMonthNameByIndex(index: Int): String {
            var month = "wrong"
            val dfs = DateFormatSymbols.getInstance(Locale("es", "ar"))
            val months = dfs.months
            if (index in 0..11) {
                month = months[index]
            }
            return month.capitalize()
        }

        fun getMonthByDate(date: String): Int {
            // MM/DD/YYYY
            return date.substring(0, 2).toInt() - 1
        }

        fun amountFormat(amount: Double): String {
            val dec = DecimalFormat("#,###.##")
            dec.roundingMode = RoundingMode.CEILING
            return dec.format(amount)
        }
    }
}