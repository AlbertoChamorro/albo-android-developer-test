fun Double.toAmount(): String {
    return Helper.amountFormat(this)
}
