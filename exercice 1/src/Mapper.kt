fun List<TransactionsResponse>.groupByMonth(): List<TransactionReporterModelView> {
    val groupingData = this
        .sortedBy {
            it.creation_date
        }
        .groupBy {
            val getMonthByDate = Helper.getMonthByDate(it.creation_date ?: "")
            Helper.getMonthNameByIndex(getMonthByDate)
        }
    return groupingData.map { item ->

        val totalIncomes = item.value.filter {
            it.status == TransactionStatus.done.toString() &&
                    it.operation == TransactionDirection.`in`.toString()
        }.map {
            it.amount ?: 0.0
        }.sum()

        val totalExpenses = item.value.filter {
            it.status == TransactionStatus.done.toString() &&
                    it.operation == TransactionDirection.`out`.toString()
        }.map {
            it.amount ?: 0.0
        }.sum()

        TransactionReporterModelView(
            month = item.key,
            pendingTransactions = item.value.count {
                it.status == TransactionStatus.pending.toString()
            },
            blockedTransaction = item.value.count {
                it.status == TransactionStatus.rejected.toString()
            },
            incomes = totalIncomes,
            expenses = totalExpenses,
            categories = item.value
                .asSequence()
                .filter {
                    it.operation == TransactionDirection.`out`.toString()
                }
                .groupBy { it.category }
                .map {
                    val totalAmountByCategory = it.value.filter { category ->
                        category.status == TransactionStatus.done.toString() &&
                                category.operation == TransactionDirection.`out`.toString()
                    }.map { category ->
                        category.amount ?: 0.0
                    }.sum()


                    TransactionCategoryModelView(
                        name = it.key ?: "",
                        percentage = ((totalAmountByCategory / totalExpenses) * 100)
                    )
                }
                .filter { it.percentage > 0.0 }
                .sortedByDescending { it.percentage }
                .toList()
        )
    }
}
