data class ApiResponse(
    var data: List<TransactionsResponse>?
)

data class TransactionsResponse(
    var uuid: Int?,
    var description: String?,
    var category: String?,
    var operation: String?,
    var amount: Double?,
    var status: String?,
    var creation_date: String?
)

data class TransactionReporterModelView(
    var month: String,
    var pendingTransactions: Int,
    val blockedTransaction: Int,
    val incomes: Double,
    val expenses: Double,
    val categories: List<TransactionCategoryModelView>
)

open class TransactionCategoryModelView(
    val name: String,
    val percentage: Double
)

enum class TransactionStatus {
    rejected,
    pending,
    done
}

enum class TransactionDirection {
    `in`,
    `out`
}